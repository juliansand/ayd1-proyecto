import { Component,ViewChild } from '@angular/core';
import { NavController, NavParams,Nav,Platform,App } from 'ionic-angular';
import {Userdata} from '../../providers/userdata'
import { AngularFire, FirebaseObjectObservable} from 'angularfire2';
import firebase from 'firebase';
import { DashboardPage } from '../dashboard/dashboard';
import { FormularioPage } from '../formulario/formulario'; 
import { PublicacionesPage } from '../publicaciones/publicaciones';
import { RestaurantesPage } from '../restaurantes/restaurantes';
import { Comercio1Page } from '../comercio1/comercio1';


/*
  Generated class for the Principal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-principal',
  templateUrl: 'principal.html',
  providers:[Userdata]
})
export class PrincipalPage {
  @ViewChild(Nav) nav:Nav;
  name:string;
  username:string;
  user:FirebaseObjectObservable<any>;
  userImage:string;
  pages: Array<{title: string, component: any}>;
  rootPage: any = DashboardPage;
  logout=false;

  constructor(public platform: Platform,public navParams: NavParams,
  public userData:Userdata,public af: AngularFire,
  public _app:App,public navCtrl:NavController) {
    this.pages=[
      {title: 'Publicaciones', component:PublicacionesPage},
      {title: 'Restaurantes', component:RestaurantesPage}
    ];
    this.user=this.userData.getUserData();
    this.user.subscribe(snap=>{
      this.username=snap.username;
      this.name=snap.acName;
      this.userImage=snap.userImage;
    });
  }

  ionViewDidLoad() {
    
  }

  openPage(page) {
  this.nav.setRoot(page.component);
  }
  salir(){
    this.logout=true; 
    this.userData.logout();
  }
  opensettings(){
      this._app.getRootNav().push(FormularioPage);
  }

  registroRestaurante(){
    this.navCtrl.push(Comercio1Page);
  }

  checkRestaurant():boolean{
    if(!this.logout){
      if(this.userData.checkRestaurant()){
        return true;
      }
      else{
        return false;
      }
    }
    else{
      return false;
    }
  }

}
