import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, ActionSheetController, LoadingController } from 'ionic-angular';
import { Restaurants } from '../../providers/restaurants';
import { DataService } from '../../providers/data.service';
import {Userdata} from '../../providers/userdata'
import { Camera,CameraOptions } from 'ionic-native';
import { PublicacionesPage } from '../publicaciones/publicaciones';

import { AngularFire, FirebaseObjectObservable,AuthProviders,AuthMethods} from 'angularfire2';
import firebase from 'firebase';
/*
  Generated class for the CreateActivity page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-create-activity',
  templateUrl: 'create-activity.html',
  providers:[Restaurants, Userdata, DataService]
})
export class CreateActivityPage {

  name;
  price;
  description;
  productKey;
  restaurante;
  currentUser;
  public downloadURL;

  constructor(public navCtrl: NavController, public navParams: NavParams, public restaurant: Restaurants, public userData:Userdata, public toastCtrl:ToastController,
  public loadingCtrl: LoadingController,
  public actionSheetCtrl: ActionSheetController,
  public dataService: DataService) {
    this.currentUser=firebase.auth().currentUser.uid;
    this.loadData(this.currentUser);
    this.productKey=this.restaurant.createProduct();
  }

  loadData(id){ 
    this.userData.viewUser(id).then(snapshot=>{
      this.restaurante= snapshot.val().restaurantes;
    });
  }

  addProduct(restaurante){
    
    let product = {
      name: this.name,
      price: this.price,
      description: this.description,
      owner: this.currentUser,
      image: this.downloadURL
    }

    this.restaurant.addProduct(product, this.productKey, restaurante).then((data)=>{
      this.presentToast("Producto Agregado");
      this.navCtrl.setRoot(PublicacionesPage);

    })
  }

  editImage(){
    var self = this;
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Seleccionar Imagen',
      buttons: [
        {
          text: 'Seleccionar imagen de Galeria',
          icon: 'image',
          handler: ()=>{
            self.openCamera(Camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Utilizar camara',
          icon: 'camera',
          handler: ()=>{
            self.openCamera(Camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  openCamera(pictureSourceType: any) {
    var self = this;

    let options: CameraOptions = {
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: pictureSourceType,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 400,
      targetHeight: 400,
      saveToPhotoAlbum: true,
      correctOrientation: true
    };

    Camera.getPicture(options).then(imageData => {
      const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          const slice = byteCharacters.slice(offset, offset + sliceSize);

          const byteNumbers = new Array(slice.length);
          for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }

          const byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
      };

      let capturedImage: Blob = b64toBlob(imageData, 'image/png');
      self.startUploading(capturedImage);
    }, error => {
      console.log('ERROR -> ' + JSON.stringify(error));
    });
  }

  startUploading(file) {

    let self = this;
    let uid = this.userData.getUserUid();
    let progress: number = 0;
    // display loader
    let loader = this.loadingCtrl.create({
      content: 'cargando..',
    });
    loader.present();

    // Upload file and metadata to the object 'images/mountains.jpg'
    var fileName = 'product-' + new Date().getTime() + '.jpg';
    var metadata = {
      contentType: 'image/png',
      name: fileName,
      cacheControl: 'no-cache',
    };

    var uploadTask = self.dataService.getStorageRef().child('images/users/' + this.currentUser + '/products/'+fileName).put(file, metadata);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on('state_changed',
      function (snapshot) {
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      }, function (error) {
        loader.dismiss().then(() => {
          switch (error.code) {
            case 'storage/unauthorized':
              // User doesn't have permission to access the object
              break;

            case 'storage/canceled':
              // User canceled the upload
              break;

            case 'storage/unknown':
              // Unknown error occurred, inspect error.serverResponse
              break;
          }
        });
      }, function () {
        loader.dismiss().then(() => {
          // Upload completed successfully, now we can get the download URL
          self.downloadURL = uploadTask.snapshot.downloadURL;
          self.presentToast("Imagen Cargada");
         
        });
      });
  }

  presentToast(message:string) {
  let toast = this.toastCtrl.create({
    message: message,
    duration: 3000,
    position: 'top'
  });

  toast.onDidDismiss(() => {
  });

  toast.present();
}

}
