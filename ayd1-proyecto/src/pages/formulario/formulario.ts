import { Component } from '@angular/core';
import { NavController, NavParams,PopoverController,ToastController,AlertController, ModalController, ActionSheetController, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AngularFire, FirebaseObjectObservable,AuthProviders,
  AuthMethods} from 'angularfire2';
import { DataService } from '../../providers/data.service';
import { Camera,CameraOptions } from 'ionic-native';
import {Userdata  } from '../../providers/userdata';
import firebase from 'firebase';

/*
  Generated class for the Formulario page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-formulario',
  templateUrl: 'formulario.html',
  providers:[Userdata, DataService]
})
export class FormularioPage {
  registroForm:FormGroup;
  submitAttempt:boolean=false;
  public userimage;
  public _userRef:any;
  public profile:any;
   public username: string;
  public nombreUsuario: string;
  public facultadUsuario;
  public generoUsuario;
  public myuserId;
  public correoUsuario:string;
  constructor(public navCtrl: NavController, 
  public navParams: NavParams, 
  public formBuilder: FormBuilder, 
  public popoverCtrl: PopoverController, 
  public Alert:AlertController, 
  private modalCtrl: ModalController,
  public userData: Userdata,
  public dataService: DataService,
  public toastCtrl:ToastController,
  public loadingCtrl: LoadingController,
  public actionSheetCtrl: ActionSheetController) {
    this.myuserId = firebase.auth().currentUser.uid;
    this.registroForm = formBuilder.group({
      nombre: ['', Validators.compose([Validators.maxLength(50), Validators.required, Validators.minLength(3)])],
      usuario: ['', Validators.compose([Validators.maxLength(20), Validators.pattern('(([._-][a-zA-Z0-9])|[a-zA-Z0-9])*'), Validators.required, Validators.minLength(5)])],
      correo: ['', Validators.compose([Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/), Validators.required])],
      nada: [''],
      nada1: [''],
    });
    
    var myuserId = firebase.auth().currentUser.uid;
    this.displayUser(myuserId);  
  }

  displayUser(theuserid){
    this.userData.viewUser(theuserid).then(snapshot=>{
      this.username=snapshot.val().username;
      this.nombreUsuario=snapshot.val().acName;
      this.correoUsuario=snapshot.val().acEmail;
      this.generoUsuario=snapshot.val().acGender;
      this.facultadUsuario=snapshot.val().acFacultad;
      this.userimage=snapshot.val().userImage;
    })
  }

  actualizarUsuario(usuario){
    this.submitAttempt=true;

    if(!this.registroForm.valid){
      console.log('NO VALIDO');
    }
    else{
      let userup={
        nombre:this.registroForm.value.nombre,
        usuario:this.registroForm.value.usuario,
        correo:this.registroForm.value.correo,
        noVacio:this.registroForm.value.noVacio
      }
      this.usernameexist(this.username);
      
      
    }
  }
usernameexist(user):any{
  this.userData.userexist(user).on('value',snapshot=>{
    var exist=snapshot.val();
    if(exist){
      this.presentToast('El usuario ya esta en uso!');
    }
    else{
      this.userData.updatePerfil(this.username, this.nombreUsuario, this.correoUsuario, this.generoUsuario, this.facultadUsuario).then((data)=>{
        this.presentToast('El perfil ha sido atualizado');
      }).catch((error)=>{
        console.log(error);
      });
    }
  });
}

presentToast(message:string) {
  let toast = this.toastCtrl.create({
    message: message,
    duration: 3000,
    position: 'top'
  });

  toast.onDidDismiss(() => {
  });

  toast.present();
}

editImage(){
    var self = this;
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Seleccionar Imagen',
      buttons: [
        {
          text: 'Seleccionar imagen de Galeria',
          icon: 'image',
          handler: ()=>{
            self.openCamera(Camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Utilizar camara',
          icon: 'camera',
          handler: ()=>{
            self.openCamera(Camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          icon: 'close',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  openCamera(pictureSourceType: any) {
    var self = this;

    let options: CameraOptions = {
      quality: 95,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: pictureSourceType,
      encodingType: Camera.EncodingType.PNG,
      targetWidth: 400,
      targetHeight: 400,
      saveToPhotoAlbum: true,
      correctOrientation: true
    };

    Camera.getPicture(options).then(imageData => {
      const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
        const byteCharacters = atob(b64Data);
        const byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          const slice = byteCharacters.slice(offset, offset + sliceSize);

          const byteNumbers = new Array(slice.length);
          for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }

          const byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
        }

        const blob = new Blob(byteArrays, { type: contentType });
        return blob;
      };

      let capturedImage: Blob = b64toBlob(imageData, 'image/png');
      self.startUploading(capturedImage);
    }, error => {
      console.log('ERROR -> ' + JSON.stringify(error));
    });
  }

  startUploading(file) {

    let self = this;
    let uid = this.userData.getUserUid();
    let progress: number = 0;
    // display loader
    let loader = this.loadingCtrl.create({
      content: 'cargando..',
    });
    loader.present();

    // Upload file and metadata to the object 'images/mountains.jpg'
    var fileName = 'userImage-' + new Date().getTime() + '.jpg';
    var metadata = {
      contentType: 'image/png',
      name: fileName,
      cacheControl: 'no-cache',
    };

    var uploadTask = self.dataService.getStorageRef().child('images/users/' + this.myuserId + '/userImage/'+fileName).put(file, metadata);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on('state_changed',
      function (snapshot) {
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      }, function (error) {
        loader.dismiss().then(() => {
          switch (error.code) {
            case 'storage/unauthorized':
              // User doesn't have permission to access the object
              break;

            case 'storage/canceled':
              // User canceled the upload
              break;

            case 'storage/unknown':
              // Unknown error occurred, inspect error.serverResponse
              break;
          }
        });
      }, function () {
        loader.dismiss().then(() => {
          // Upload completed successfully, now we can get the download URL
          var downloadURL = uploadTask.snapshot.downloadURL;
          self.userData.setUserImage(downloadURL);
          self.presentToast(downloadURL);
          self.reload();
            //self.dataService.setUserImage(uid);
        });
      });
  }

  reload(){
    this.userData.getUserData().subscribe(snap=>{
      this.userimage=snap.userImage;   
    });
  }


}
