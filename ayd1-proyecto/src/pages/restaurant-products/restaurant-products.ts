import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Restaurants } from '../../providers/restaurants';
import { MapPage } from '../map/map';

import firebase from 'firebase';

import { ProductDetailPage } from '../product-detail/product-detail';

/*
  Generated class for the RestaurantProducts page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-restaurant-products',
  templateUrl: 'restaurant-products.html',
  providers:[Restaurants]
})
export class RestaurantProductsPage {

  producto;
  products = [];
  restaurante=[];
  rest;
  kiosco;

  //datos del restaurante
  direccion;
  imagen;
  nombre;
  web;
  lat;
  lng;
  categoria;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public restaurant: Restaurants) {
    this.rest = this.navParams.get('key');
    this.loadProducts();
    this.loadRestaurant(this.rest);
  }

  loadRestaurant(key){
    this.restaurant.viewRestaurant(key).then(snapshot=>{
      this.direccion=snapshot.val().direccion;
      this.imagen=snapshot.val().imagen;
      this.nombre=snapshot.val().nombre;
      this.web=snapshot.val().web;
      this.lat=snapshot.val().lat;
      this.lng=snapshot.val().lng;
      this.categoria=snapshot.val().lng;
    })
  }

  loadProducts(){
      this.producto=firebase.database().ref('/productos/');
      this.producto.on('value', snapshot=>{
        let list = [];
        snapshot.forEach(snap=>{
          list.push({
            key: snap.key,
            name: snap.val().name,
            price:snap.val().price,
            description: snap.val().description,
            image:snap.val().image,
            owner: snap.val().owner
          });
        });
        this.products=list;
    });
  }

  location(){
    this.navCtrl.push(MapPage, {
      lat: this.lat, lng: this.lng
    })
  }


  viewDetail(id){
    this.navCtrl.push(ProductDetailPage, {
      id: id
    })
  }

  validate(product):boolean{
    console.log(this.rest, product);
    firebase.database().ref('/restaurantes/'+this.rest+'/products').on('value', snapshot=>{
      let list = [];
      snapshot.forEach(snap=>{
        list.push({
          id:snap.val().id,
          key: snap.key
        });
        return false;
      })
      this.restaurante=list;
    })

    let p = false;
    for(let i=0; i<this.restaurante.length; i++){
      if(product = this.restaurante[i].id){
        p = true;
      }
    }
    
    return p;
  }

}
