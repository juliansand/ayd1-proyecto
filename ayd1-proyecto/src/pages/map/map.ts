import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';



@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})


export class MapPage {

  lat;
  lng;

  constructor(public navCtrl: NavController, public navParams: NavParams, private googlemaps: GoogleMaps) {
    this.lat=this.navParams.get('lat');
    this.lng=this.navParams.get('lng');
    console.log(this.lat, this.lng)
  }

  ngAfterViewInit() {
    this.loadMap();
  }

  loadMap(){
    let element: HTMLElement = document.getElementById('map');
    let map: GoogleMap = this.googlemaps.create(element);
    map.one(GoogleMapsEvent.MAP_READY).then(()=>console.log("mapa listo"));

    let ubicacion : LatLng = new LatLng(43.0741904,-89.3809802);

    let position: CameraPosition = {
      target:ubicacion,
      zoom:18,
      tilt:30
    };

    map.moveCamera(position);

    let markerOPtion: MarkerOptions = {
      position: ubicacion,
      title: 'A'
    };

    map.addMarker(markerOPtion)
      .then((marker: Marker)=>{
        marker.showInfoWindow();
      });
   
  }
}
