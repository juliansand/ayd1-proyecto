import { Component } from '@angular/core';
import { NavController, NavParams,ToastController } from 'ionic-angular';
import { PrincipalPage } from '../principal/principal';
import {Userdata} from '../../providers/userdata'
import {AngularFire} from 'angularfire2';
import firebase from 'firebase';


@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
  providers:[Userdata]
})


export class RegistroPage {
  public userData:Userdata;
  public userRef;
  public email:string;
  public password:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public af: AngularFire, public users: Userdata, private toastCtrl: ToastController) {
    this.userRef = firebase.database().ref('/usuarios');
    this.userData=users;
  }


  crearCuenta(){
    var credentials = {email:this.email, password:this.password}
    this.userData.createUser(credentials).then((data)=>{
      this.presentToast('Usuario Registrado con Exito');
    }).catch((error)=>{
      console.log(error);
      this.presentToast("Error en el registro");
    })
  }


presentToast(message:string) {
  let toast = this.toastCtrl.create({
    message: message,
    duration: 3000,
    position: 'top'
  });
  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });
  toast.present();
}
}
