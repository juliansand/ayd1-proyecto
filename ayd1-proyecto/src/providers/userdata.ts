import { Injectable } from '@angular/core';
import { AngularFire, FirebaseObjectObservable,AuthProviders,AuthMethods} from 'angularfire2';
import firebase from 'firebase';

/*
  Generated class for the Userdata provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Userdata {
  public currentUser: any;
  public fireAuth: any;
  user;
  userauth;
  userdata;

  constructor(public af: AngularFire) {
    this.fireAuth=firebase.auth();
    this.userdata = firebase.database().ref('/users/');
  }

  getUserUid():any{
    this.currentUser = firebase.auth().currentUser.uid;
    return this.currentUser;
  }

  createUser(credentials){
    return new Promise((resolve: ()=> void, reject: (reason:Error)=> void)=>{
      firebase.auth().createUserWithEmailAndPassword(credentials.email, credentials.password)
      .then((authData)=>{
        this.userauth=authData;
        this.user=credentials;
        this.createUserData();
        resolve();
      }).catch((error)=>{
        reject(error);
      });
    });
  }

  createUserData(){
    var data = {
      datecreated: firebase.database['ServerValue']['TIMESTAMP'],
      acEmail: this.user.email,
      username: 'None',
      acName: 'None',
      acGender: 'f',
      acFacultad: 'ing',
      userimage: 'https://firebasestorage.googleapis.com/v0/b/lunchbook-df7e0.appspot.com/o/images%2FdefaultImages%2Favatar-default.png?alt=media&token=a61e4adc-bdcd-4895-8a38-2b12775f1133'
    };
    this.userdata.child(this.userauth.uid).update(data);
  }

  updatePerfil(username: string, nombre: string, correo: string, genero, facultad): firebase.Promise<any>{
  this.currentUser = firebase.auth().currentUser.uid;
  var userRef = this.userdata.child(this.currentUser);
  return userRef.set({
    username: username,
    acName: nombre,
    acEmail: correo,
    acGender: genero,
    acFacultad: facultad,
  });
}

  getUserData():FirebaseObjectObservable<any> { 
    this.currentUser = firebase.auth().currentUser.uid;
    const thisuser$ : FirebaseObjectObservable<any> = this.af.database.object('/users/' + this.currentUser); 
    return thisuser$;
  }

  login(credentials) {
    let providerConfig = {
      provider: AuthProviders.Password,
      method: AuthMethods.Password
    };
    return new Promise((resolve: () => void, reject: (reason: Error) => void) => {
      this.af.auth.login({email: credentials.email,password: credentials.password},providerConfig)
      .then((authData) => {
        this.userauth = authData;
        resolve();
      }).catch((error) => {
        reject(error);
      });
    });
  }
  loginUser(credentials): any {
    return this.fireAuth.signInWithEmailAndPassword(credentials.email,credentials.password);
  }

  logout() {
    return firebase.auth().signOut();
  }

  setUserImage(image){
    this.currentUser = firebase.auth().currentUser.uid;
    this.userdata.child(this.currentUser).child('userImage').set(image);
  }

  viewUser(userId){
    var userRef = this.userdata.child(userId);
    return userRef.once('value');
  }

  userexist(usuario){
    return this.userdata.orderByChild("username").equalTo(usuario);
  }

  setRestImage(image){
    this.currentUser = firebase.auth().currentUser.uid;
    this.userdata.child(this.currentUser).child('restaurantImage').set(image);
  }

  checkRestaurant():boolean{
    this.currentUser = firebase.auth().currentUser.uid;  
    let refuser=firebase.database().ref('/users/'+this.currentUser+'/restaurantes');
    let empty=false;
    refuser.on('value',snapshot=>{
      if(snapshot.val()){
        empty= false;
      }
      else{
        empty= true;
      }
    }); 
    return empty;
  }

}
