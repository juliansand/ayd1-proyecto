import { Component, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import firebase from 'firebase';
import {
  FIREBASE_PROVIDERS, defaultFirebase,
  AngularFire, firebaseAuthConfig
} from 'angularfire2';

import { PrincipalPage } from '../pages/principal/principal';
import { HomePage } from '../pages/home/home';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  zone: NgZone;
  usuario = "hola";
  password="1234";
  nombre;

  private hello: string = 'Hello, World!';

  constructor(platform: Platform, af:AngularFire) {

    this.zone = new NgZone({});

    firebase.auth().onAuthStateChanged((user)=>{
      this.zone.run(()=>{
        if(!user){
          this.rootPage=HomePage;
          console.log("Prueba de Carga Exitosa");
        }else{
          this.rootPage=PrincipalPage;
          console.log("Prueba de Login Exitosa");
          this.prueba2(user.uid);
          console.log(this.nombre);
        }
      });
    });

    this.prueba1();
   

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  prueba1(){
    if(this.usuario==='hola' && this.password==='1234'){
      console.log("Unit Test 'Prueba1' Successfull");
    }
    else{
      console.log("Unit Test Failed");
    }
  }

  prueba2(user){
    firebase.database().ref('/users/'+user).once('value', snapshot=>{
      this.nombre=snapshot.val().acName;
    });

    if(this.nombre === 'None'){
      console.log("Unit Test 'Register' Failed");
    }else{
      console.log("Unit Test 'Register' Successfull");
    }

  }

  prueba3(){
    firebase.database().ref('/prueba/').once('value', snapshot=>{
      let username = snapshot.val().nombre;
      if(username === 'favio'){
        let apellido = snapshot.val().apellido;
        if(apellido === 'gonzalez'){
          console.log("Unit Test 'Data' Successfull");
        }else{
          console.log("Unit Test 'Data' Failed");
        }
      }else{
        console.log("Unit Test 'Data' Failed");
      }
    });
  }

  prueba4(){
    firebase.database().ref('/Menu/').once('value', snapshot=>{
      let list = [];
       snapshot.forEach(snap=>{
        
          list.push({
          key:snap.key
        })
        return false;
        
    });

    if ( list.length === 0 ){
      console.log("Unit Test 'Menu de platillos' Failed");
    }
    else {
      console.log("Unit Test 'Menu de platillos' Successfull");
    }
    });
  }

  
  
}
